interface language {
  key: string;
  title: string;
}

interface ErrorInfo {
  display: boolean;
  status?: string;
  text?: string;
}

interface systemState {
  currentLang: language;
  [mode: string]: unknown;
}

type systemMutation = {
  (state: systemState, value: unknown): void;
};

interface systemMutationsList {
  [mutations: string]: systemMutation;
}

interface PopupValue {
  display: boolean;
  text?: string;
  status?: string;
}

interface authState {
  [all: string]: any;
}

export { language, systemState, systemMutation, ErrorInfo, systemMutationsList, authState, PopupValue };
