import { Commit, Dispatch } from 'vuex';
import { httpClient, httpStatsClient, getNonce, signIn, getProjects, getUserInfo, updateUserInfo } from '@/api';
import { getNonceResponse, signInResponse } from '@/api/types';
import { authState } from '@/store/types';
import Cookies from 'js-cookie';
import { web3 } from '@/web3';
import router from '@/router';

export default {
  state: {
    walletKey: '',
    isAuthorized: false,
    isAdmin: false,
    authToken: '',

    userProjects: [],
    currentApiKey: 'initial',
    userInfo: null,
    userLoading: false,

    analytics: {},

    currentNetwork: {
      title: 'Ethereum',
      key: 'ETH',
      chainId: 1,
    },

    adminLoading: false,
  } as authState,

  getters: {
    keyAlias(state: authState) {
      if (!state.walletKey) return '';
      if (state.walletKey.length !== 0) {
        return state.walletKey.slice(0, 5) + '...' + state.walletKey.slice(state.walletKey.length - 4);
      }
      return '';
    },

    keyAliasMobile(state: authState) {
      if (state.walletKey.length !== 0) {
        return state.walletKey.slice(0, 2) + '...';
      }
      return '';
    },
  },

  mutations: {
    setUserInfo(state, value) {
      state.userInfo = value;
    },

    setUserLoading(state, value) {
      state.userLoading = value;
    },

    setAuthorized(state: authState, value: boolean) {
      state.isAuthorized = value;
    },

    setWalletKey(state: authState, value: string) {
      state.walletKey = value;
      state.isAuthorized = Boolean(value);

      if (value) Cookies.set('flashWalletKey', value);
      else Cookies.remove('flashWalletKey');
    },

    setAuthToken(state: authState, value: string) {
      state.authToken = value ? value : null;
      httpClient.defaults.headers.common['Authorization'] = value ? `Bearer ${value}` : null;
      httpStatsClient.defaults.headers.common['Authorization'] = value ? `Bearer ${value}` : null;

      if (value && !Cookies.get('flashAuthToken')) Cookies.set('flashAuthToken', value);
      if (!value) Cookies.remove('flashAuthToken');
    },

    setUserProjects(state, value) {
      state.userProjects = value;
    },

    setAnalytics(state, value) {
      state.analytics = value;
    },

    setCurrentNetwork(state, value) {
      state.currentNetwork = value;
    },

    setAdminLoading(state, value) {
      state.adminLoading = value;
    },

    setCurrentApiKey(state, value) {
      state.currentApiKey = value;
    },
  },

  actions: {
    async linkWallet({ state, commit, dispatch }: { state: authState; commit: Commit; dispatch: Dispatch }) {
      commit('setWalletConnecting', true);
      try {
        let walletKey, nonce, signedNonce, authToken, userInfo;

        try {
          const walletRes = await window.ethereum.request({ method: 'eth_requestAccounts' });
          walletKey = walletRes[0];

          window.ethereum.on('accountsChanged', (accounts) => {
            if (!accounts.includes(walletRes[0])) dispatch('logOut');
          });
          window.ethereum.on('chainChanged', (chainId) => {
            if (chainId !== state.currentNetwork.chainId) dispatch('logOut');
          });
        } catch (err) {
          if (err.name === 'TypeError') dispatch('setErrorPopupDisplay', { display: true, text: 'no-meta' });
          else dispatch('setErrorPopupDisplay', { display: true, text: 'reject-connect' });
          throw err;
        }

        try {
          await switchNetwork(state.currentNetwork);
        } catch (err) {
          // if (err.code !== 4902 && err.code !== 32603)
          dispatch('setErrorPopupDisplay', { display: true, text: 'miss-network' });
          // else dispatch('setErrorPopupDisplay', { display: true, text: 'reject-switch' });
          throw err;
        }

        try {
          const nonceRes: getNonceResponse = await getNonce(walletKey);
          nonce = nonceRes.data.nonce;
        } catch (err) {
          dispatch('setErrorPopupDisplay', { display: true, text: 'nonce-error' });
          throw err;
        }

        try {
          signedNonce = await web3.eth.personal.sign(nonce, walletKey, 'test');
        } catch (err) {
          dispatch('setErrorPopupDisplay', { display: true, text: 'deny-sign' });
          throw err;
        }

        try {
          const loginRes: signInResponse = await signIn({ public_address: walletKey, signed_nonce: signedNonce });
          authToken = loginRes.data.access_token;
        } catch (err) {
          dispatch('setErrorPopupDisplay', { display: true, text: 'sign-fail' });
          throw err;
        }
        commit('setAuthToken', authToken);
        window.dataLayer = window.dataLayer || [];
        window.dataLayer.push({
          event: 'new_login',
        });

        await dispatch('getUserInfo');

        if (!state.userInfo?.full_name) await updateUserInfo({ full_name: 'No name' });

        if (!state.userInfo?.email || !state.userInfo?.full_name) commit('setAlmostThereDisplay', true);

        commit('setWalletKey', walletKey);
        commit('setConnectModalDisplay', false);
        dispatch('getAccountInfo');
      } catch (err) {
        console.error(err);
      } finally {
        commit('setWalletConnecting', false);
      }
    },

    async getUserProjects({ commit }: { commit: Commit }) {
      commit('setProjectsLoading', true);
      try {
        const projectsRes = await getProjects();
        commit('setUserProjects', projectsRes.data.results);
      } catch (err) {
        console.error(err);
        console.error('[GetProjects Error]');
      } finally {
        commit('setProjectsLoading', false);
      }
    },

    async getUserInfo({ commit }: { commit: Commit }) {
      commit('setUserLoading', true);
      try {
        const userRes = await getUserInfo();
        commit('setUserInfo', userRes.data);
      } catch (err) {
        console.error(err);
      } finally {
        commit('setUserLoading', false);
      }
    },

    getAccountInfo({ dispatch }: { dispatch: Dispatch }) {
      dispatch('getUserProjects');
      dispatch('getUserInfo');
    },

    logOut({ commit }: { commit: Commit }) {
      commit('setAuthToken', null);
      commit('setWalletKey', null);
      commit('setAnalytics', { chart: [] });
      commit('setUserProjects', []);
      commit('setUserInfo', null);
      commit('setCurrentApiKey', '');

      if ((router as any).history.current.name !== 'index') router.push('/');
    },
  },
};

async function switchNetwork(currentNetwork) {
  if (window.ethereum.networkVersion !== currentNetwork.chainId)
    await window.ethereum.request({
      method: 'wallet_switchEthereumChain',
      params: [{ chainId: web3.utils.toHex(currentNetwork.chainId) }],
    });
}
