import { language, systemState, systemMutationsList, ErrorInfo, PopupValue } from '@/store/types';
import { disableBodyScroll, enableBodyScroll } from 'body-scroll-lock';

function manageScroll(display: boolean, el: Element) {
  if (display) disableBodyScroll(el);
  else enableBodyScroll(el);
}

// new comment 

export default {
  state: {
    darkMode: true,
    isSettingsVisible: false,
    almostThereDisplay: false,

    errorPopupDisplay: false,
    errorPopupStatus: '',
    errorPopupText: 'no-meta',
    errorPopupTimeout: null as null | number,

    infoModalDisplay: false,
    infoModalText: 'invoice-sent',

    theConfiguratorDisplay: false,
    connectModalDisplay: false,
    renewDisplay: false,
    accountModalDisplay: false,
    languageSelectDisplay: false,
    invoiceModalDisplay: false,

    isWalletConnecting: false,

    projectsLoading: false,

    popupDisplay: false,
    popupInfo: '',
    popupInfoStatus: 'info' as 'info' | 'warn' | 'error',
    popupTimeout: null as number | null,

    currentLang: {
      key: 'en',
      title: 'English',
    },
  } as systemState,

  mutations: {
    setDarkMode(state, value: boolean) {
      state.darkMode = value;
    },

    setSettingsDisplay(state, value: boolean) {
      state.isSettingsVisible = value;
      const el = document.querySelector('#panelSettings');
      manageScroll(value, el);
    },

    setCurrentLang(state, value: language) {
      state.currentLang = { ...value };
    },

    setAlmostThereDisplay(state, value: boolean) {
      state.almostThereDisplay = value;
      const el = document.querySelector('#almostThereContent');
      manageScroll(value, el);
    },

    setInvoiceModalDisplay(state, value: boolean) {
      state.invoiceModalDisplay = value;
      const el = document.querySelector('#invoiceModalContent');
      manageScroll(value, el);
    },

    setInfoModalDisplay(state, value: { display: boolean; text: string }) {
      state.infoModalText = value.text ? value.text : state.infoModalText;
      state.infoModalDisplay = value.display;
      const el = document.querySelector('#infoModalContent');
      manageScroll(value.display, el);
    },

    setErrorPopupDisplay(state, value: ErrorInfo) {
      state.errorPopupDisplay = value.display;
      state.errorPopupStatus = value.status ? value.status : 'error';
      state.errorPopupText = value.text ? value.text : '';
    },

    setTheConfiguratorDisplay(state, value: boolean) {
      state.theConfiguratorDisplay = value;
      const el = document.querySelector('#theConfiguratorContent');
      manageScroll(value, el);
    },

    setConnectModalDisplay(state, value: boolean) {
      state.connectModalDisplay = value;
      const el = document.querySelector('#connectModal');
      manageScroll(value, el);
    },

    setWalletConnecting(state, value: boolean) {
      state.isWalletConnecting = value;
    },

    setRenewDisplay(state, value: boolean) {
      state.renewDisplay = value;
      // const el = document.querySelector('#renewContent');
      // manageScroll(value, el);
    },

    setAccountModalDisplay(state, value: boolean) {
      state.accountModalDisplay = value;
    },

    setLanguageSelectDisplay(state, value: boolean) {
      state.languageSelectDisplay = value;
    },

    setProjectsLoading(state, value) {
      state.projectsLoading = value;
    },

    setPopupDisplay(state, value: PopupValue) {
      state.popupInfo = value.text;
      state.popupInfoStatus = value.status ? value.status : 'info';
      state.popupDisplay = value.display;
    },
  } as systemMutationsList,

  actions: {
    setErrorPopupDisplay({ state, commit }, value) {
      if (value.display === false) {
        state.errorPopupDisplay = false;
        return;
      }

      if (value.display === true && state.errorPopupDisplay === true) {
        state.errorPopupDisplay = false;
        if (state.errorPopupTimeout) clearTimeout(state.errorPopupTimeout);
        state.errorPopupTimeout = setTimeout(() => {
          commit('setErrorPopupDisplay', value);
        }, 100);
        return;
      }

      commit('setErrorPopupDisplay', value);
    },

    setPopupDisplay({ state, commit }, value) {
      if (value.display === false) {
        state.popupDisplay = false;
        return;
      }

      if (value.display === true && state.popupDisplay === true) {
        state.popupDisplay = false;
        if (state.popupTimeout) clearTimeout(state.popupTimeout);
        state.popupTimeout = setTimeout(() => {
          commit('setPopupDisplay', value);
        }, 100);
        return;
      }

      commit('setPopupDisplay', value);
    },
  },
};
