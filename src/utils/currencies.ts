export default [
  {
    full_name: 'Bitcoin',
    symbol: 'BTC',
    details: 'Bitcoin',
  },
  {
    full_name: 'Ethereum',
    symbol: 'ETH',
    details: 'Ethereum',
  },
  {
    full_name: 'BNB Smart Chain',
    symbol: 'BSC',
    details: 'BNB Smart Chain',
  },
  {
    full_name: 'Solana',
    symbol: 'SOL',
    details: 'Solana',
  },
  {
    full_name: 'Aptos',
    symbol: 'APT',
    details: 'Aptos',
  },
  {
    full_name: 'Avalanche',
    symbol: 'AVAX',
    details: 'Avalanche',
  },
  {
    full_name: 'Polygon',
    symbol: 'MATIC',
    details: 'Polygon',
  },
];
