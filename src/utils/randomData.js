export default [
  {
    date: new Date(2022, 9, 15),
    value: 2340,
  },
  {
    date: new Date(2022, 9, 16),
    value: 9023,
  },
  {
    date: new Date(2022, 9, 17),
    value: 3741,
  },
  {
    date: new Date(2022, 9, 18),
    value: 1102,
  },
  {
    date: new Date(2022, 9, 19),
    value: 1598,
  },
  {
    date: new Date(2022, 9, 20),
    value: 4219,
  },
];
