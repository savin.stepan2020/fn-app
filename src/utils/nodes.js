export default [
  {
    key: 'eth',
    title: 'Ethereum',
    mode: 'Full',
    network: 'Mainnet',
    link: 'https://de.flashnodes.io/eth/mainnet/a665a45920422f9d417e4867efdc4fb8a04a1f3fff1fa07e998e86f7f7a27ae3',
    code: `curl -X POST https://rpc.ankr.com/bsc -d '{"jsonrpc":"2.0","method": "eth_blockNumber","params":[],"id":1}a665a45920422f9d417e4867efdc4fb8a04a1f3fff1fa07e998e86f7f7a27ae3a665a45920422f9d417e4867efdc4fb8a04a1f3fff1fa07e998e86f7f7a27ae3`,
  },
  {
    key: 'bsc',
    title: 'BNB Smart Chain',
    mode: 'Full',
    network: 'Mainnet',
    link: 'https://de.flashnodes.io/bsc/mainnet/a665a45920422f9d417e4867efdc4fb8a04a1f3fff1fa07e998e86f7f7a27ae3',
    code: `curl -X POST https://rpc.ankr.com/bsc -d '{"jsonrpc":"2.0","method": "eth_blockNumber","params":[],"id":1}'`,
  },
];
