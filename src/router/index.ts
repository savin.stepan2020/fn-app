import Vue from 'vue';
import VueRouter, { RouteConfig } from 'vue-router';
import Index from '@/views/Index.vue';
import EndpointPage from '@/views/EndpointPage.vue';
import Admin from '@/views/Admin.vue';
import AdminProject from '@/views/AdminProject.vue';
import NotFound from '@/views/NotFound.vue';
import { getAdminProjects } from '@/api/admin';

Vue.use(VueRouter);

const adminRoutes: Array<RouteConfig> = [
  {
    path: '/admin',
    name: 'admin',
    component: Admin,
  },
  {
    path: '/admin/project/:id',
    name: 'admin-project',
    component: AdminProject,
  },
];

const routes: Array<RouteConfig> = [
  {
    path: '/',
    name: 'index',
    component: Index,
  },
  {
    path: '/endpoint/:key',
    name: 'endpoints',
    component: EndpointPage,
  },
  {
    path: '*',
    name: 'not-found',
    component: NotFound,
  },
  ...adminRoutes,
];

const router = new VueRouter({
  mode: 'history',
  base: process.env.BASE_URL,
  routes,
});

router.beforeEach(async (to, from, next) => {
  if (adminRoutes.map((route) => route.name).includes(to.name)) {
    try {
      const res = await getAdminProjects();
      next();
    } catch {
      next('/not-found');
    }
  } else next();
});

export default router;
