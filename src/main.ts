import Vue from 'vue';
import App from './App.vue';
import router from './router';
import store from './store';
import VueI18n from 'vue-i18n';
import device from 'vue-device-detector';
import vClickOutside from 'v-click-outside';

import './assets/styles/index.scss';
import './assets/styles/fonts.scss';

import en from './locales/en.json';
import cn from './locales/cn.json';
import ru from './locales/ru.json';

Vue.config.productionTip = false;

Vue.use(vClickOutside);

Vue.use(VueI18n);
Vue.use(device);

const i18n = new VueI18n({
  locale: 'en',
  fallbackLocale: 'en',
  messages: {
    en,
    cn,
    ru,
  },
});

new Vue({
  router,
  store,
  i18n,
  render: (h) => h(App),
}).$mount('#app');
