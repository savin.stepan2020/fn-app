declare module 'body-scroll-lock' {
  export function disableBodyScroll(el: any): void;
  export function enableBodyScroll(el: any): void;
}
