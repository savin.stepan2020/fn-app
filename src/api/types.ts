interface getNonceResponse {
  data: {
    nonce: string;
    public_address: string;
  };
}

interface signInResponse {
  data: {
    access_token: string;
    nonce: string;
    token_type: string;
  };
}

export { getNonceResponse, signInResponse };
