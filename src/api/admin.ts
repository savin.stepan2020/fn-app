import Cookies from 'js-cookie';
import { httpClient } from '@/api';

const token = Cookies.get('flashAuthToken');
httpClient.defaults.headers.common['Authorization'] = token ? `Bearer ${token}` : null;

function getAdminProjects(params = { limit: 10, offset: 0 }) {
  return httpClient.get('/api/v1/admin/projects', { params });
}

function manageProject(id, params) {
  return httpClient.post(`/api/v1/admin/projects/manage/${id}`, null, { params });
}

function deleteProject(id) {
  return httpClient.delete(`/api/v1/admin/projects/${id}`);
}

function createProject(publicAddress, data) {
  return httpClient.post(`/api/v1/admin/projects/request/${publicAddress}`, data);
}

function getCurrencies() {
  return httpClient.get('/api/v1/admin/cryptocurrencies');
}

function createCurrency(data) {
  return httpClient.post('/api/v1/admin/cryptocurrencies', data);
}

function editCurrency(id, data) {
  return httpClient.put(`/api/v1/admin/cryptocurrencies/${id}`, data);
}

function deleteCurrency(id) {
  return httpClient.delete(`/api/v1/admin/cryptocurrencies/${id}`);
}

function getSuperUsers() {
  return httpClient.get('/api/v1/admin/superuser');
}

function addSuperUser(id) {
  return httpClient.post(`/api/v1/admin/superuser/${id}`);
}

function deleteSuperUser(id) {
  return httpClient.delete(`/api/v1/admin/superuser/${id}`);
}

export {
  getAdminProjects,
  manageProject,
  deleteProject,
  createProject,
  getCurrencies,
  createCurrency,
  editCurrency,
  deleteCurrency,
  getSuperUsers,
  addSuperUser,
  deleteSuperUser,
};
