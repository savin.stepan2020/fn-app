import currencies from '@/utils/currencies';
import axios from 'axios';

const httpClient = axios.create({
  baseURL: process.env.VUE_APP_API_URL,
});

const httpStatsClient = axios.create({
  baseURL: process.env.VUE_APP_API_STATS_URL,
});

function getNonce(address) {
  return httpClient.get(`/api/v1/login/metamask/nonce/${address}`);
}

function signIn(data) {
  return httpClient.post('/api/v1/login/auth', data);
}

function getUserInfo() {
  return httpClient.get('/api/v1/users/me');
}

function updateUserInfo(data) {
  return httpClient.put('/api/v1/users/me', data);
}

function getProjects() {
  return httpClient.get('/api/v1/projects');
}

function getProject(id) {
  return httpClient.get(`/api/v1/projects/` + id);
}

function createProject(data) {
  return httpClient.post('/api/v1/projects/request', data);
}

function getAnalytics(params) {
  return httpStatsClient.get('/chart', {
    params: {
      selector: 'api_key',
      ...params,
    },
  });
}

function createCurrencies() {
  currencies.forEach((coin) => {
    httpClient.post('/api/v1/admin/cryptocurrencies', coin);
  });
}

function test() {
  return httpClient.get('/api/v1/admin/projects/0xA08bC556C72B568994E9Aef9127bEe4613E6Fe3e');
}

export {
  httpClient,
  httpStatsClient,
  getNonce,
  signIn,
  getProjects,
  getProject,
  getUserInfo,
  updateUserInfo,
  createProject,
  getAnalytics,
  createCurrencies,
  test,
};
