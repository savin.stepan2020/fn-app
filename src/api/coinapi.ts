import axios from 'axios';

const coinAPIClient = axios.create({
  baseURL: process.env.VUE_APP_COIN_API_URL,
  // headers: {
  //   'X-CoinAPI-Key': '883F52CB-3854-4696-A677-CE22BFDF3E82',
  // },
});

function getCurrencyInfo(key: string) {
  return coinAPIClient.get('/query', {
    params: {
      function: 'CURRENCY_EXCHANGE_RATE',
      from_currency: key,
      to_currency: 'USDT',
      apikey: 'TAFQMHH1IZKB04VF',
    },
  });
}

export { coinAPIClient, getCurrencyInfo };
