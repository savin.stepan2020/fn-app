<template>
  <div class="currency-card grid gap-6 items-center p-4">
    <div class="flex items-center justify-between xs:w-full">
      <div class="flex items-center gap-4">
        <div v-if="false" class="flex justify-center items-center relative">
          <img width="64" height="64" src="@/assets/images/views/index/polygon.svg" />
          <img
            width="48"
            height="48"
            :src="require(`@/assets/images/coins/${currency.symbol.toLowerCase()}.svg`)"
            class="absolute inset-0 m-auto shrink-0"
          />
        </div>
        <p class="text-lg leading-5 font-bold text-txtdprim w-full overflow-hidden overflow-ellipsis whitespace-nowrap">
          {{ currency.full_name }}
        </p>
      </div>
      <div
        class="bg-btndsec h-8 px-2 flex justify-center items-center text-sm font-medium text-txtdsec rounded-12 uppercase"
      >
        {{ currency.symbol }}
      </div>
    </div>
    <div class="flex items-center gap-4 xs:hidden">
      <div style="height: 45px" class="w-0.5 bg-brdddef"></div>
      <div class="w-full">
        <p class="text-sm font-medium text-txtdtert">Details</p>
        <p
          class="leading-5 font-bold text-txtdprim capitalize whitespace-nowrap overflow-hidden overflow-ellipsis w-full"
        >
          {{ currency.details }}
        </p>
      </div>
    </div>
    <div class="flex items-center gap-5">
      <UiButton color-type="secondary" button-height="sm" class="ml-auto rounded-16 w-24" @click="editCurrency">
        Edit
      </UiButton>
      <UiButton button-height="sm" class="rounded-16 w-24" @click="deleteCurrency">
        <span v-if="!loading">Delete</span> <LoadingSpinner color-type="white" v-else />
      </UiButton>
    </div>
  </div>
</template>

<script lang="ts">
import { deleteCurrency } from '@/api/admin';

import LoadingSpinner from '@/components/ui/LoadingSpinner.vue';
import UiButton from '@/components/ui/UiButton.vue';

export default {
  name: 'CurrencyCard',

  components: { UiButton, LoadingSpinner },

  props: {
    currency: {
      type: Object,
      default: () => ({}),
    },
  },

  data() {
    return {
      loading: false,
    };
  },

  methods: {
    editCurrency() {
      this.$emit('edit-currency', this.currency);
    },

    async deleteCurrency() {
      this.loading = true;

      try {
        await deleteCurrency(this.currency.id);
        this.$store.dispatch('setPopupDisplay', {
          display: true,
          text: 'Currency successfully deleted.',
        });
        this.$emit('update-currencies');
      } catch (err) {
        console.error(err);
        this.$store.dispatch('setPopupDisplay', {
          display: true,
          text: 'Error while deleting currency.',
          status: 'error',
        });
      } finally {
        this.loading = false;
      }
    },
  },
};
</script>

<style lang="scss">
.currency-card {
  grid-template-columns: 220px 400px 1fr;
}
</style>
