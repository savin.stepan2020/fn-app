export default {
  data() {
    return {
      coins: [
        {
          key: 'ETH',
          title: 'Ethereum',
        },
        {
          key: 'BTC',
          title: 'Bitcoin',
        },
        {
          key: 'BSC',
          title: 'BNB Smart Chain',
        },
        {
          key: 'SOL',
          title: 'Solana',
        },
        {
          key: 'APT',
          title: 'Aptos',
        },
        {
          key: 'AVAX',
          title: 'Avalanche',
        },
        {
          key: 'MATIC',
          title: 'Polygon',
        },
      ],

      networks: [
        {
          title: 'Mainnet',
          key: 'mainnet',
        },
        {
          title: 'Testnet',
          key: 'testnet',
        },
      ],

      modes: [
        {
          title: 'Full',
          key: 'full',
        },
        {
          title: 'Archive',
          key: 'archived',
        },
      ],

      periods: [
        {
          title: '3 days',
          key: '3d',
        },
        {
          title: '1 month',
          key: '1m',
          value: 1,
        },
        {
          title: '3 months',
          key: '3m',
          value: 3,
        },
      ],

      renewPeriods: [
        {
          title: '1 month',
          key: '1m',
          value: 1,
        },
        {
          title: '3 months',
          key: '3m',
          value: 3,
        },
        {
          title: '6 months',
          key: '6m',
          value: 6,
        },
        {
          title: '9 months',
          key: '9m',
          value: 9,
        },
      ],
    };
  },
};
