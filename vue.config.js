const { defineConfig } = require('@vue/cli-service');
module.exports = defineConfig({
  transpileDependencies: true,

  devServer: {
    // proxy: 'https://api.flashnodes.io/',
    proxy: {
      '^/api/': {
        target: 'https://api.flashnodes.io/',
        changeOrigin: true,
      },
      '^/chart': {
        target: 'https://stats.flashnodes.io/',
        changeOrigin: true,
      },
    },
  },

  configureWebpack: {
    externals: function (context, request, callback) {
      if (/xlsx|canvg|pdfmake/.test(request)) {
        return callback(null, 'commonjs ' + request);
      }
      callback();
    },
  },
});
